/**
 * @file
 */
(function () {

  'use strict';
  // All the JavaScript for this file.
  $('.m-click').click(function () {
    $('.mobile_menu').slideToggle('slow');
  });
})();
